//{{{
/*

	SDL2_rotozoom.c: rotozoomer, zoomer and shrinker for 32bit or 8bit surfaces

	Copyright (C) 2012  Andreas Schiffler

	This software is provided 'as-is', without any express or implied
	warranty. In no event will the authors be held liable for any damages
	arising from the use of this software.

	Permission is granted to anyone to use this software for any purpose,
	including commercial applications, and to alter it and redistribute it
	freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented; you must not
	claim that you wrote the original software. If you use this software
	in a product, an acknowledgment in the product documentation would be
	appreciated but is not required.

	2. Altered source versions must be plainly marked as such, and must not be
	misrepresented as being the original software.

	3. This notice may not be removed or altered from any source
	distribution.

	Andreas Schiffler -- aschiffler at ferzkopp dot net

*/

/*

	This file contains altered code.
	I (Ewen Goisot) only kept the functions I needed.
	I needed a function similar to `zoomSurface`,
	but only for a part of the surface,
	with similar execution time.

	Functions I modified now assume it's 32 bits and smooth and no flip.

	Feel free to ask me to add clarifications about what you can do with this.

	Ewen Goisot -- ewengoisot@laposte.net -- @ewen-goisot:matrix.org

*/
//}}}

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#define GUARD_ROWS (2)
#define VALUE_LIMIT     0.001

typedef struct tColorRGBA {
	Uint8 r;
	Uint8 g;
	Uint8 b;
	Uint8 a;
} tColorRGBA;

int _zoomSurfaceRGBA(SDL_Surface * src, SDL_Surface * dst, SDL_Rect * rect)
{
	int x, y, sx, sy, ssx, ssy, *sax, *say, *csax, *csay, *salast, csx, csy, ex, ey, cx, cy, sstep, sstepx, sstepy;
	//int sstep_rect;
	tColorRGBA *c00, *c01, *c10, *c11;
	tColorRGBA *sp, *csp, *dp;
	int spixelgap, spixelw, spixelh, dgap, t1, t2;
	/* Allocate memory for row/column increments */
	if ((sax = (int *) malloc((dst->w + 1) * sizeof(Uint32))) == NULL) { return (-1); }
	if ((say = (int *) malloc((dst->h + 1) * sizeof(Uint32))) == NULL) { free(sax); return (-1); }
	/* Precalculate row increments */
	spixelw = (rect->w - 1);
	spixelh = (rect->h - 1);
	sx = (int) (65536.0 * (float) spixelw / (float) (dst->w - 1));
	sy = (int) (65536.0 * (float) spixelh / (float) (dst->h - 1));
	/* Maximum scaled source size */
	//ssx = (rect->w << 16) - 1;
	//ssy = (rect->h << 16) - 1;
	ssx = (src->w << 16) - 1;
	ssy = (src->h << 16) - 1;
	/* Precalculate horizontal row increments */
	csx = 0;
	csax = sax;
	for (x = 0; x <= dst->w; x++) {
		*csax = csx;
		csax++;
		csx += sx;
		/* Guard from overflows */
		if (csx > ssx) { csx = ssx; }
	}
	/* Precalculate vertical row increments */
	csy = 0;
	csay = say;
	for (y = 0; y <= dst->h; y++) {
		*csay = csy;
		csay++;
		csy += sy;
		/* Guard from overflows */
		if (csy > ssy) { csy = ssy; }
	}

	sp = (tColorRGBA *) src->pixels;
	dp = (tColorRGBA *) dst->pixels;
	sp += rect->x + src->w * rect->y;
	//printf("starts at %d + %d * %d\n", rect->x, src->w, rect->y);
	dgap = dst->pitch;
	spixelgap = src->pitch/4;

	/* Interpolating Zoom */
	csay = say;
	for (y = 0; y < dst->h; y++) {
		csp = sp;
		csax = sax;
		for (x = 0; x < dst->w; x++) {
			/* Setup color source pointers */
			ex = (*csax & 0xffff);
			ey = (*csay & 0xffff);
			cx = (*csax >> 16);
			cy = (*csay >> 16);
			sstepx = cx < spixelw;
			sstepy = cy < spixelh;
			c00 = sp;
			c01 = sp;
			c10 = sp;
			if (sstepy) { c10 += spixelgap; }
			c11 = c10;
			if (sstepx) { c01++; c11++; }

			/* Draw and interpolate colors */
			t1 = ((((c01->r - c00->r) * ex) >> 16) + c00->r) & 0xff;
			t2 = ((((c11->r - c10->r) * ex) >> 16) + c10->r) & 0xff;
			dp->r = (((t2 - t1) * ey) >> 16) + t1;
			t1 = ((((c01->g - c00->g) * ex) >> 16) + c00->g) & 0xff;
			t2 = ((((c11->g - c10->g) * ex) >> 16) + c10->g) & 0xff;
			dp->g = (((t2 - t1) * ey) >> 16) + t1;
			t1 = ((((c01->b - c00->b) * ex) >> 16) + c00->b) & 0xff;
			t2 = ((((c11->b - c10->b) * ex) >> 16) + c10->b) & 0xff;
			dp->b = (((t2 - t1) * ey) >> 16) + t1;
			t1 = ((((c01->a - c00->a) * ex) >> 16) + c00->a) & 0xff;
			t2 = ((((c11->a - c10->a) * ex) >> 16) + c10->a) & 0xff;
			dp->a = (((t2 - t1) * ey) >> 16) + t1;
			/* Advance source pointer x */
			salast = csax;
			csax++;
			sstep = (*csax >> 16) - (*salast >> 16);
			sp += sstep;

			/* Advance destination pointer x */
			dp++;
		}
		/* Advance source pointer y */
		salast = csay;
		csay++;
		sstep = (*csay >> 16) - (*salast >> 16);
		sstep *= spixelgap;
		sp = csp + sstep;

		/* Advance destination pointer y */
		//dp = (tColorRGBA *) ((Uint8 *) dp + dgap);
	}

	/* Remove temp arrays */
	free(sax);
	free(say);

	return (0);
}

void zoomSurfaceSize(int width, int height, double zoomx, double zoomy, int *dstwidth, int *dstheight)
{
	/* Sanity check zoom factors */
	if (zoomx < VALUE_LIMIT) { zoomx = VALUE_LIMIT; }
	if (zoomy < VALUE_LIMIT) { zoomy = VALUE_LIMIT; }

	/* Calculate target size */
	*dstwidth  = (int) floor(((double) width  * zoomx) + 0.5);
	*dstheight = (int) floor(((double) height * zoomy) + 0.5);
	if (*dstwidth  < 1) { *dstwidth  = 1; }
	if (*dstheight < 1) { *dstheight = 1; }
	//printf("zoom: w=%d, h=%d\n", *dstwidth, *dstheight);
}


SDL_Surface *zoomSurface(SDL_Surface * src, double zoomx, double zoomy, SDL_Rect * rect)
{
	SDL_Surface *rz_src;
	SDL_Surface *rz_dst;
	int dstwidth, dstheight;
	int src_converted;

	/* Sanity check */
	if (src == NULL) return (NULL);
	if (rect->x + rect->w > src->w || rect->y + rect->h > src->h) {
		printf("trop gros, fait un regime\n");
		return NULL;
	}
	/* Use source surface 'as is' */
	rz_src = src;
	src_converted = 0;

	/* Get size if target */
	zoomSurfaceSize(rect->w, rect->h, zoomx, zoomy, &dstwidth, &dstheight);

	/* Alloc space to completely contain the zoomed surface */
	rz_dst = NULL;
	/* Target surface is 32bit with source RGBA/ABGR ordering */
	rz_dst =
		SDL_CreateRGBSurface(SDL_SWSURFACE, dstwidth, dstheight + GUARD_ROWS, 32,
				rz_src->format->Rmask, rz_src->format->Gmask,
				rz_src->format->Bmask, rz_src->format->Amask);
	/* Check target */
	if (rz_dst == NULL) {
		/* Cleanup temp surface */
		if (src_converted) {
			SDL_FreeSurface(rz_src);
		}
		return NULL;
	}

	/* Adjust for guard rows */
	rz_dst->h = dstheight;
	/* Lock source surface */
	if (SDL_MUSTLOCK(rz_src)) { SDL_LockSurface(rz_src); }
	/* Check which kind of surface we have (or not) */
	_zoomSurfaceRGBA(rz_src, rz_dst, rect);
	/* Unlock source surface */
	if (SDL_MUSTLOCK(rz_src)) { SDL_UnlockSurface(rz_src); }
	/* Cleanup temp surface */
	if (src_converted) { SDL_FreeSurface(rz_src); }
	/* Return destination surface */
	return (rz_dst);
}
