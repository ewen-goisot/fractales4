//{{{PREPROCESSOR
//#include <iostream>
//using namespace std;
#include <stdio.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h> //ne pas surcharger le terminal
//#include <SDL2/SDL2_rotozoom.h> //OpenGL, for antialiazed zoom
#include <time.h> //hasard (init), chrono
//#include <vector> //heap memory is larger
#include <math.h> //fmod, cos
//compil_1: `g++ mainnew.cpp -O3 -ffast-math -lSDL2 -lSDL2_image -lSDL2_ttf -o a.out`
//compil_2: `g++ mainnew.cpp -O3 -ffast-math -lSDL2 -lSDL2_image -lSDL2_ttf -DROTATION_MODE_MULTIPLY -o a.out_nomul`
//!./a.out -n 600 -v 2 -d 100 -r 6 -m 1.4142135623731
//TODO C isof C++
//bc<<<"scale=30;1+
//1/(2+1/(1+1/(1+1/(2+1/(1+1/(2+1/(2+1/(1+1/(1+1/(2+1/(2+1/(1+1/(2+1/(1+1/(1+1/(2+
//1/(1+1/(2+1/(2+1/(1+1/(2+1/(1+1/(1+1/(2+1/(2+1/(1+1/(1+1/(2+1/(1+1/(2+1/(2+1/(1+
//1/(1+1/(2+1/(2+1/(1+1/(2+1/(1+1/(1+1/(2+1/(2+1/(1+1/(1+1/(2+1/(1+1/(2+1/(2+1/(1+
//1/(2+1/(1+1/(1+1/(2+1/(1+1/(2+1/(2+1/(1+1/(1+1/(2+1/(2+1/(1+1/(2+1/(1+1/(1+1/(2+
//1))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))"
#include "zoom_translate.c"

#define SPEED_MAX 1'000'000'000
#define SPEED_MULTIPLIER 200
#define TITRE "fractal modulo"
#define PIXEL(i, j) (pixels[(i) + (j)*l])

#define min(c, d) ({ \
	 __typeof__ (c) _c = (c); \
	 __typeof__ (d) _d = (d); \
	 _c < _d ? _c : _d; })
#define max(c, d) ({ \
	 __typeof__ (c) _c = (c); \
	 __typeof__ (d) _d = (d); \
	 _c > _d ? _c : _d; })
#define fast_min(c, d) (((c) < (d)) ? (c) : (d))
#define fast_max(c, d) (((c) > (d)) ? (c) : (d))
//#define ROTATION_MODE_MULTIPLY
//}}}

int main(int argc, char *argv[]){
	//{{{ ARGC, ARGV
	[[maybe_unused]] int i, j, k, kk, a, b;
	[[maybe_unused]] double aux_double;
	[[maybe_unused]] char *aux_char;

	int u_d = 100; //frames, milliseconds
	int u_n = 680; //board size
	int u_p = 1;
	int u_q = 4;
	int u_r = 0;
	int u_s = 1;
	int u_u = 1;
	int u_v = 1; //steps per frame (with 4FPS)
	int u_w = 200;
	long double u_m = (1+sqrt(5))/2; //modulo
	long long int u_t = 1.000e+10;
	long long int u_T = 0;
	double u_z=0;
	char s_output_path[64]; //TODO boundaries
	char s_output_name[256];
	char s_formule[24] = "udf";
	[[maybe_unused]] bool b_imag = false;
	for(i=0; i<argc; i++){
		if(argv[i][0] == '-'){
			if(i+1==argc || argv[i][1]=='\0'){ perror("user syntax error\n"); return 127; }
			i++;
			switch(argv[i-1][1]){
				case 'd': u_d = atoi(&argv[i][0]); break; //frames, milliseconds
				case 'n': u_n = atoi(&argv[i][0]); break; //DEPRECIATED board size (unnecessary : 680)
				case 'p': u_p = atoi(&argv[i][0]); break; //numérateur rotation de référence
				case 'q': u_q = atoi(&argv[i][0]); break; //dénominateur : on tourne de p/q
				case 'r': u_r = atoi(&argv[i][0]); break; //rotation initiale : r/q
				case 's': u_s = atoi(&argv[i][0]); break; //on regarde ts%m
				case 'u': u_u = atoi(&argv[i][0]); break; //subpixels precision
				case 'v': u_v = atoi(&argv[i][0]); break; //initial speed (unnecessary : 2)
				case 'w': u_w = atoi(&argv[i][0]); break; //speed multiplier
				case 'm': u_m = strtold(&argv[i][0], &aux_char); break; //if ts%m>1, rotate ±p/q (or t*p/q)
				case 't': u_t = (long long int) atof(&argv[i][0]); break; //last step
				case 'T': u_T = (long long int) atof(&argv[i][0]); break; //first frame
				case 'z': u_z = atof(&argv[i][0]); break; //initial zoom
				case 'i': sprintf(s_formule, "%s", argv[i]); break;
				case 'o': sprintf(s_output_path, "%s", argv[i]);
						  sprintf(s_output_name, "[ -d %s ]||mkdir %s", argv[i], argv[i]);
						  a = system(s_output_name);
						  if(a!=0) { perror("mkdir error\n"); return a; }
						  b_imag = true;
						  break;
				default: perror("user syntax error\n"); return 127;
			}
		}
	}
	if (u_z==0) { u_z = u_q==4 ? 1.0 : 0.5; }
	//}}}

	//{{{ VARIABLES DECLARATION
	[[maybe_unused]] const long double m = u_m;
	const int n = u_n;
	const int u = u_u;
	[[maybe_unused]] const int p = u_p;
	[[maybe_unused]] const int q = u_q;
	[[maybe_unused]] const int r = u_r;
	[[maybe_unused]] const int s = u_s;
	const int f_l = 2*n, l = u*f_l;
	const int f_h = 1*n, h = u*f_h;
	int v = u_v;
	int n_frame=0;
	const int frame_temps = u_d;
	[[maybe_unused]] bool b_exit = false; //doit-on quitter le programme ka
	[[maybe_unused]] bool b_chro = false;
	[[maybe_unused]] bool b_play = false;
	[[maybe_unused]] bool b_step = false;
	[[maybe_unused]] bool b_tiny = true; //aucun resize n'a été fait
	[[maybe_unused]] int chrono_temps; //nb microsecondes par v étapes
	[[maybe_unused]] long long int t = 0; //nb étapes

	double z = u_z; // niveau de zoom
	double x = l*z/2.0; //coordonnée horizontale
	double y = h*z/2.0; //coordonnée verticale
	int xi=(int)(x/z), yi=(int)(y/z); //versions entières de x, y
	int i_min=xi, i_max=xi, j_min=yi, j_max=yi, i_min_aux=xi, i_max_aux=xi, j_min_aux=yi, j_max_aux=yi;
	int w = r%q;
	//}}}
	printf("init %f %f\n", x, y);
	//cout<<"init"<<x<<" "<<y<<endl;

	//{{{ SDL INITIALISATION
	[[maybe_unused]] SDL_Window* win = nullptr;
	[[maybe_unused]] SDL_Renderer* renderer = nullptr;
	[[maybe_unused]] SDL_Texture* texture = nullptr;
	[[maybe_unused]] SDL_Surface* surface = nullptr;
	[[maybe_unused]] SDL_Surface* surface_screen = nullptr; //antialiased, SDL_gfx library rewritten
	[[maybe_unused]] SDL_Surface* surface_output = nullptr; //antialiased, SDL_gfx library rewritten
	[[maybe_unused]] SDL_Surface* surface_bis = nullptr; //for TTF
	[[maybe_unused]] Uint32* pixels = nullptr; //from surface
	[[maybe_unused]] Uint32* pixels_screen_size = nullptr; //from surface_screen
	[[maybe_unused]] Uint32* pixels_translated_CAFE = nullptr; //from surface_translated_CAFE
	[[maybe_unused]] SDL_Rect rect; rect.w=1; rect.h=1; //calculs
	[[maybe_unused]] SDL_Rect rect_bis; rect.w=1; rect.h=1; //visuel
	[[maybe_unused]] SDL_Event event;
	[[maybe_unused]] TTF_Font* font = nullptr;
	[[maybe_unused]] Uint32 c; //color aux
	[[maybe_unused]] SDL_Color couleur[3] = {{0x00,0x00,0x00,0xff}, {0xff,0xff,0xff,0xff}, {0x80,0x80,0x80,0xff}};
	[[maybe_unused]] char s_current_state[256]; //for TTF TODO have less strings
	if (SDL_Init(SDL_INIT_VIDEO) != 0){ perror("SDL_Init Error\n"); return 1; }
	//if (SDL_Init(SDL_INIT_VIDEO) != 0){ std::cerr << "SDL_Init Error: " << SDL_GetError() << std::endl; return 1; }
	win = SDL_CreateWindow(TITRE, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, f_l, f_h, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
	if (!win) { perror("SDL_Window Error\n"); return 2; }
	//if (!win) { std::cerr << "SDL_Window Error: " << SDL_GetError() << std::endl; return 2; }
	renderer = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xff);
	SDL_RenderClear(renderer);
	SDL_RenderPresent(renderer);
	surface = SDL_CreateRGBSurface(0, l, h, 32, 0, 0, 0, 0); //if u>1: larger than the screen
	surface_output = SDL_CreateRGBSurface(0, 1366, 734, 32, 0, 0, 0, 0); //if u>1: larger than the screen
	pixels = (Uint32 *) surface->pixels;
	TTF_Init();
	font = TTF_OpenFont("ttf/DejaVuSansMono.ttf", 16);
	//}}}

	//{{{ARRAY INITIALISATION
	double rules_x[q];
	double rules_y[q];
	for (k=0; k<q; k++) {
		rules_x[k] = cos( ((k*p*2)*M_PI) / q );
		rules_y[k] = sin( ((k*p*2)*M_PI) / q );
	}
	//[[maybe_unused]] double rule_xx = cos( (k*2*M_PI) / q );
	//[[maybe_unused]] double rule_xy = sin( (k*2*M_PI) / q );
	//[[maybe_unused]] double rule_yx = - rule_xy; //-sin
	//[[maybe_unused]] double rule_yy = rule_xx; //cos
	[[maybe_unused]] double situation;
	unsigned char fast_cos[0x1000];
	for (k=0; k<0x1000; k++) {
		fast_cos[k] = (unsigned char) (0x8f + int(0x6f.ffp0 * cos((M_PI*k)/0x0800)));
	}
	//= [127+int(127.99*math.cos(2*pi*i/2000)) for i in range(2000)]

	// initialiser la map
	for(i=0; i<l; i++){
		for(j=0; j<h; j++){
			PIXEL(i, j) = 0;
		}
	}
	//}}}
	while(SDL_PollEvent(&event)){} // vide les événements
	while(1){
		chrono_temps = clock();
		if((b_play || b_step) && n_frame%100==0 && t!=0){ printf("step: %lld frame: %d\n", t, n_frame); }
		//if((b_play || b_step) && n_frame%100==0 && t!=0){ cout << "step: " << t << " frame: " << n_frame << endl; }

		//{{{TINY, FIRST STEPS
		if (b_tiny) {
			for (k=0; k < (b_play ? v : (b_step?1:0)); k++) {
				c = 0xff<<0x18; //alpha
				t++;
				a = (int) (log(t)*0x100000);
				b = fast_cos[(a/0x100)   & 0x0fff];
				c |=   b<<0x10; //red
				b = fast_cos[(a/0x10)    & 0x0fff];
				c |=   b<<0x08; //green
				b = fast_cos[(a/0x1)     & 0x0fff];
				c |=   b<<0x00; //blue
				situation = fmod(t*s, m);
				if (situation > 1) {
#ifdef ROTATION_MODE_MULTIPLY
					//w=((-w%q)+q+2*t)%q;
					w+=t%q; w+=q; w%=q;
#else
					w += 1 - 2*(t%2); w+=q; w%=q;
#endif
				}
				x -= rules_x[w];
				y -= rules_y[w];
				xi = (int) (x/z);
				yi = (int) (y/z);
				i_min_aux = min(i_min_aux, xi);
				j_min_aux = min(j_min_aux, yi);
				i_max_aux = max(i_max_aux, xi);
				j_max_aux = max(j_max_aux, yi);
				if(xi<128 | yi<128 | xi>=l-128 | yi>=h-128) {
					b_tiny=false;
					printf("notiny %d %d %lld %d->%d x %d->%d\n", xi, yi, t, i_min_aux, i_max_aux, j_min_aux, j_max_aux);
					//cout<<"notiny"<<xi<<" "<<yi<<" "<<t<<" "<<i_min_aux<<" "<<i_max_aux<<" "<<j_min_aux<<" "<<j_max_aux<<" "<<endl;
					i_min = i_min_aux;
					i_max = i_max_aux;
					j_min = j_min_aux;
					j_max = j_max_aux;
					break;
				}
				if (z<1 &&  u==4 && ((i_max_aux-i_min_aux >= 1.5*f_l) || (j_max_aux-j_min_aux >= 1.5*f_h))) {
					xi/=2; yi/=2;
					printf("tiny_sorti: %lld %d->%d x %d->%d, count: %d\n", t, i_min, i_max, j_min, j_max, a);
					//cout<<"tiny_sorti: "<<t<<" "<<i_min<<"->"<<i_max<<" x "<<j_min<<"->"<<j_max<<" count:"<<a<<endl;
					for(i=max(0, i_min_aux/2-1); i<i_max_aux+1; i++){
						for(j=max(0, j_min_aux/2-1); j<j_max_aux+1; j++){
							if(2*i<l-1 && 2*j<h-1){
								PIXEL(i, j) = PIXEL(2*i, 2*j) ? PIXEL(2*i, 2*j) :
									PIXEL(2*i+1, 2*j) ? PIXEL(2*i+1, 2*j) :
									PIXEL(2*i, 2*j+1) ? PIXEL(2*i, 2*j+1) : PIXEL(2*i+1, 2*j+1);
							} else {
								PIXEL(i, j) = 0; //vide
							}
						}
					}
					z*=2;
					i_min=l; j_min=h; i_max=0; j_max=0;
					for (i=max(0, i_min_aux/2-1); i<min(l, i_max_aux/2+1); i++) {
						for (j=max(0, j_min_aux/2-1); j<min(h, j_max_aux/2+1); j++) {
							if (PIXEL(i, j)) {
								i_min = min(i_min, i);
								j_min = min(j_min, j);
								i_max = max(i_max, i);
								j_max = max(j_max, j);
							}
						}
					}
					i_min_aux = i_min;
					i_max_aux = i_max;
					j_min_aux = j_min;
					j_max_aux = j_max;
					// too few light
				}
				PIXEL(xi, yi) = c;
				//if (t==8000) {
				//b_play=false;
				//break;
				//}
				//cout<<"tiny"<<b<<endl;
			}
		}
		//}}}

		if(not b_tiny){
			//if (t>=4100) {
				////cout<<"ici"<<endl;
				//b_play=false;
			//}
			for (k=0; k < (b_play ? v : (b_step?1:0)); k+=16) {
				//{{{CALCULS 1/16 : POSITION, COULEUR
					//cout<<"notiny"<<xi<<" "<<yi<<" "<<t<<" "<<i_min_aux<<" "<<i_max_aux<<" "<<j_min_aux<<" "<<j_max_aux<<" "<<endl;
				i_min_aux = min(i_min_aux, xi);
				j_min_aux = min(j_min_aux, yi);
				i_max_aux = max(i_max_aux, xi);
				j_max_aux = max(j_max_aux, yi);
					//cout<<"notiny"<<xi<<" "<<yi<<" "<<t<<" "<<i_min_aux<<" "<<i_max_aux<<" "<<j_min_aux<<" "<<j_max_aux<<" "<<endl;
				c = 0xff<<0x18; //alpha
				a = (int) (log(t)*0x100000);
				b = fast_cos[(a/0x100)   & 0x0fff];
				c |=   b<<0x10; //red
				b = fast_cos[(a/0x10)    & 0x0fff];
				c |=   b<<0x08; //green
				b = fast_cos[(a/0x1)     & 0x0fff];
				c |=   b<<0x00; //blue
				//}}}
				for (kk=0; kk<16; kk++) {
					//{{{CALCULS 1/1 : DÉPLACEMENTS
					t++;
					situation = fmod(t*s, m);
					if (situation > 1) {
#ifdef ROTATION_MODE_MULTIPLY
						//w=((-w%q)+q+2*t)%q;
						w+=t%q; w+=q; w%=q;
#else
						w += 1 - 2*(t%2); w+=q; w%=q;
#endif
					}
					x -= rules_x[w];
					y -= rules_y[w];
					xi = (int) (x/z);
					yi = (int) (y/z);
					//cout<<"notiny"<<xi<<" "<<yi<<" "<<t<<" "<<i_min_aux<<" "<<i_max_aux<<" "<<j_min_aux<<" "<<j_max_aux<<" "<<endl;
					//}}}
					//{{{ OUT OF MAP CORRECTION
					if(xi<4 | yi<2 | xi>=l-4 | yi>=h-2) {
						//cout<<"map=-1 at "<<xi<<" "<<yi<<endl;
						//i_min=l-1; j_min=h-1; i_max=0; j_max=0;
						i_min = i_min_aux;
						j_min = j_min_aux;
						i_max = i_max_aux;
						j_max = j_max_aux;
						for (i=max(i_min_aux-17, 0); i<min(i_max_aux+18, l); i++) {
							for (j=max(j_min_aux-17, 0); j<j_min_aux+1; j++) {
								if (PIXEL(i, j)) {
									i_min = min(i_min, i);
									j_min = min(j_min, j);
									i_max = max(i_max, i);
									j_max = max(j_max, j);
								}
							}
							for (j=j_max_aux; j<min(j_max_aux+18, h); j++) {
								if (PIXEL(i, j)) {
									i_min = min(i_min, i);
									j_min = min(j_min, j);
									i_max = max(i_max, i);
									j_max = max(j_max, j);
								}
							}
						}
						for (j=max(j_min_aux-17, 0); j<min(j_max_aux+18, h); j++) {
							for (i=max(i_min_aux-17, 0); i<i_min_aux+1; i++) {
								if (PIXEL(i, j)) {
									i_min = min(i_min, i);
									j_min = min(j_min, j);
									i_max = max(i_max, i);
									j_max = max(j_max, j);
								}
							}
							for (i=i_max_aux; i<min(i_max_aux+18, l); i++) {
								if (PIXEL(i, j)) {
									i_min = min(i_min, i);
									j_min = min(j_min, j);
									i_max = max(i_max, i);
									j_max = max(j_max, j);
								}
							}
						}
						//i_min=l-1; j_min=h-1; i_max=0; j_max=0;
						//for (i=max(i_min_aux-17, 0); i<min(i_max_aux+18, l); i++) {
						//for (j=max(j_min_aux-17, 0); j<min(j_max_aux+18, h); j++) {
						//if (PIXEL(i, j)) {
						//i_min = min(i_min, i);
						//j_min = min(j_min, j);
						//i_max = max(i_max, i);
						//j_max = max(j_max, j);
						//}
						//}
						//}
						i_min_aux = i_min;
						i_max_aux = i_max;
						j_min_aux = j_min;
						j_max_aux = j_max;
						if (l+i_min-i_max<64 || h+j_min-j_max<32) {
							xi/=2; yi/=2;
							printf("sorti: %lld %d->%d x %d->%d, count: %d\n", t, i_min, i_max, j_min, j_max, a);
							//cout<<"sorti: "<<t<<" "<<i_min<<"->"<<i_max<<" x "<<j_min<<"->"<<j_max<<" count:"<<a<<endl;
							for(i=max(0, i_min_aux/2-1); i<i_max_aux+1; i++){
								for(j=max(0, j_min_aux/2-1); j<j_max_aux+1; j++){
									if(2*i<l-1 && 2*j<h-1){
										PIXEL(i, j) = PIXEL(2*i, 2*j) ? PIXEL(2*i, 2*j) :
											PIXEL(2*i+1, 2*j) ? PIXEL(2*i+1, 2*j) :
											PIXEL(2*i, 2*j+1) ? PIXEL(2*i, 2*j+1) : PIXEL(2*i+1, 2*j+1);
									} else {
										PIXEL(i, j) = 0; //vide
									}
								}
							}
							z*=2;
							SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
							SDL_RenderClear(renderer);
							i_min=l; j_min=h; i_max=0; j_max=0;
							for (i=max(0, i_min_aux/2-1); i<min(l, i_max_aux/2+1); i++) {
								for (j=max(0, j_min_aux/2-1); j<min(h, j_max_aux/2+1); j++) {
									if (PIXEL(i, j)) {
										i_min = min(i_min, i);
										j_min = min(j_min, j);
										i_max = max(i_max, i);
										j_max = max(j_max, j);
									}
								}
							}
							i_min_aux = i_min;
							i_max_aux = i_max;
							j_min_aux = j_min;
							j_max_aux = j_max;
						} else {
							printf("reste: %lld %d->%d x %d->%d, count: %d\n", t, i_min, i_max, j_min, j_max, a);
							//cout<<"reste: "<<t<<" "<<i_min<<"->"<<i_max<<" x "<<j_min<<"->"<<j_max<<" count:"<<a<<endl;
						}
						if (l+i_min-i_max>=64 && h+j_min-j_max>=32) {
							a = (l-i_min-i_max)/2; x+=a*z; xi+=a;
							b = (h-j_min-j_max)/2; y+=b*z; yi+=b;
							i_min_aux += a;
							i_max_aux += a;
							j_min_aux += b;
							j_max_aux += b;
							if(b<0){
								for (j=j_min; j<j_max+1; j++) {
									for (i=i_min; i<i_max+1; i++) {
										PIXEL(i+a, j+b) = PIXEL(i, j);
										PIXEL(i, j) = 0;
									}
								}
							} else if(b>0){
								for (j=j_max; j>j_min-1; j--) {
									for (i=i_min; i<i_max+1; i++) {
										PIXEL(i+a, j+b) = PIXEL(i, j);
										PIXEL(i, j) = 0;
									}
								}
							} else if(a<0){
								for (i=i_min; i<i_max+1; i++) {
									for (j=j_min; j<j_max+1; j++) {
										PIXEL(i+a, j+b) = PIXEL(i, j);
										PIXEL(i, j) = 0;
									}
								}
							} else if(a>0){
								for (i=i_max; i>i_min-1; i--) {
									for (j=j_min; j<j_max+1; j++) {
										PIXEL(i+a, j+b) = PIXEL(i, j);
										PIXEL(i, j) = 0;
									}
								}
							} else { printf("bug\n"); }
							//} else { cout<<"bug"<<endl; }
						} else {
							printf("bug\n");
							//cout<<"bug"<<endl;
						}
						xi = min(max(xi, 0), l);
						yi = min(max(yi, 0), h);
					}
					//}}}
					PIXEL(xi, yi) = c;
				}
			}
		}
		if(b_play) { v = max(v, min((int)(u_v+t/u_w), SPEED_MAX)); }

		//affichage //{{{
		if(b_play || b_step){
			if (b_tiny) {
				i_min = i_min_aux;
				j_min = j_min_aux;
				i_max = i_max_aux;
				j_max = j_max_aux;
			} else { //get exact mins and maxs, only 1/16 were evaluated before
				//i_min=l-1; j_min=h-1; i_max=0; j_max=0;
				i_min = i_min_aux;
				j_min = j_min_aux;
				i_max = i_max_aux;
				j_max = j_max_aux;
				for (i=max(i_min_aux-17, 0); i<min(i_max_aux+18, l); i++) {
					for (j=max(j_min_aux-17, 0); j<j_min_aux+1; j++) {
						if (PIXEL(i, j)) {
							i_min = min(i_min, i);
							j_min = min(j_min, j);
							i_max = max(i_max, i);
							j_max = max(j_max, j);
						}
					}
					for (j=j_max_aux; j<min(j_max_aux+18, h); j++) {
						if (PIXEL(i, j)) {
							i_min = min(i_min, i);
							j_min = min(j_min, j);
							i_max = max(i_max, i);
							j_max = max(j_max, j);
						}
					}
				}
				for (j=max(j_min_aux-17, 0); j<min(j_max_aux+18, h); j++) {
					for (i=max(i_min_aux-17, 0); i<i_min_aux+1; i++) {
						if (PIXEL(i, j)) {
							i_min = min(i_min, i);
							j_min = min(j_min, j);
							i_max = max(i_max, i);
							j_max = max(j_max, j);
						}
					}
					for (i=i_max_aux; i<min(i_max_aux+18, l); i++) {
						if (PIXEL(i, j)) {
							i_min = min(i_min, i);
							j_min = min(j_min, j);
							i_max = max(i_max, i);
							j_max = max(j_max, j);
						}
					}
				}
				//cout<<"before"<<t<<" x"<<xi<<" y"<<yi<<" t"<<t<<" ia"<<i_min_aux<<" ib"<<i_max_aux<<" ja"<<j_min_aux<<" jb"<<j_max_aux<<endl;
				i_min_aux = i_min; i_min -= i_min%2;
				i_max_aux = i_max; i_max += 2-i_max%2; if(i_max==l) { i_max=l-2; }
				j_min_aux = j_min; j_min -= j_min%2;
				j_max_aux = j_max; j_max += 2-j_max%2; if(j_max==h) { j_max=h-2; }
				//cout<<"after"<<t<<" x"<<xi<<" y"<<yi<<" t"<<t<<" ia"<<i_min_aux<<" ib"<<i_max_aux<<" ja"<<j_min_aux<<" jb"<<j_max_aux<<endl;
			}
			rect.x=i_min-2; rect.y=j_min-1; rect.w=i_max-i_min+5; rect.h=j_max-j_min+3;
			aux_double = min((float)f_l/rect.w, (float)f_h/rect.h);
			surface_screen = zoomSurface(surface, aux_double, aux_double, &rect);
			rect.x = 0;
			rect.y = 0;
			if(surface_screen->w < f_l) {
				rect.w = surface_screen->w;
				rect.h = f_h;
				rect_bis.x = 3+(f_l-surface_screen->w)/2;
				rect_bis.y = 42;
			} else if(surface_screen->h < f_h) {
				rect.w = f_l;
				rect.h = surface_screen->h;
				rect_bis.x = 3;
				rect_bis.y = 42+(f_h-surface_screen->h)/2;
			} else if(surface_screen->w==f_l && surface_screen->h==f_h) {
				rect.w = f_l;
				rect.h = f_h;
				rect_bis.x = 3;
				rect_bis.y = 42;
			} else {
				printf("bugsize\n");
				//cout<<"bugsize"<<endl;
				rect.w = f_l;
				rect.h = f_h;
				rect_bis.x = 3;
				rect_bis.y = 42;
			}
			rect_bis.w = rect.w;
			rect_bis.h = rect.h;
			//cout<<"afte "<<rect.x<<" "<<rect.y<<" "<<rect.w<<" "<<rect.h<<" "<<endl;
			texture = SDL_CreateTextureFromSurface(renderer, surface_screen);
			SDL_RenderClear(renderer);
			SDL_RenderCopy(renderer, texture, &rect, &rect_bis);
			//if (b_imag) {
				//SDL_BlitSurface(surface_screen, &rect, surface_output, &rect_bis);
			//}
			SDL_FreeSurface(surface_screen);
			if(texture != nullptr) { SDL_DestroyTexture(texture); }

			if(s==1) {
#ifdef ROTATION_MODE_MULTIPLY
				sprintf(s_current_state, "step: %.3e    SPF: %.3e    size: %.3e x %.3e    test: t %% %.8f    formula: %s    turn: t * %d/%d",
						(float)t, (float)v, (z)*(i_max_aux-i_min_aux+1), (z)*(j_max_aux-j_min_aux+1), (double)m, s_formule, p, q);
#else
				sprintf(s_current_state, "step: %.3e    SPF: %.3e    size: %.3e x %.3e    test: t %% %.8f    formula: %s    turn: %d/%d",
						(float)t, (float)v, (z)*(i_max_aux-i_min_aux+1), (z)*(j_max_aux-j_min_aux+1), (double)m, s_formule, p, q);
#endif
			} else {
#ifdef ROTATION_MODE_MULTIPLY
				sprintf(s_current_state, "step: %.3e    SPF: %.3e    size: %.3e x %.3e    test: %d*t %% %.8f    formula: %s    turn: t * %d/%d",
						(float)t, (float)v, (z)*(i_max_aux-i_min_aux+1), (z)*(j_max_aux-j_min_aux+1), s, (double)m, s_formule, p, q);
#else
				sprintf(s_current_state, "step: %.3e    SPF: %.3e    size: %.3e x %.3e    test: %d*t %% %.8f    formula: %s    turn: %d/%d",
						(float)t, (float)v, (z)*(i_max_aux-i_min_aux+1), (z)*(j_max_aux-j_min_aux+1), s, (double)m, s_formule, p, q);
#endif
			}

			surface_bis = TTF_RenderUTF8_Blended(font, s_current_state, couleur[1]);
			texture = SDL_CreateTextureFromSurface(renderer, surface_bis);
			SDL_QueryTexture(texture, nullptr, nullptr, &rect.w, &rect.h);
			rect.x=0; rect.y=0;
			rect_bis.x=(1366-rect.w)/2; rect_bis.y=3; rect_bis.w=rect.w; rect_bis.h=rect.h;
			SDL_RenderFillRect(renderer, &rect_bis);
			SDL_RenderCopy(renderer, texture, &rect, &rect_bis);
			//if (b_imag) {
				//SDL_BlitSurface(surface_bis, &rect, surface_output, &rect_bis);
			//}
			if(texture != nullptr) { SDL_DestroyTexture(texture); }
			if(surface_bis != nullptr) { SDL_FreeSurface(surface_bis); }


			SDL_RenderPresent(renderer);
			if (b_imag && n_frame>u_T) {
				sprintf(s_output_name, "%s/%05d.png", s_output_path, n_frame);
				SDL_RenderReadPixels(renderer, nullptr, surface_output->format->format, surface_output->pixels, surface_output->pitch);
				IMG_SavePNG(surface_output, s_output_name);
			}
				n_frame++;
		}
		b_step = false;
		//}}}

		//raccourcis clavier //{{{
		while(SDL_PollEvent(&event)){
			if(event.type==SDL_QUIT){
				b_exit = true;
				break;
			}
			if(event.type==SDL_KEYDOWN){
				switch (event.key.keysym.sym) {
					case 'q': //quit
						b_exit = true;
						break;
						//TODO don't change current speed but (almost const) t/v
					case 'a':
						v = max(v/2, 1);
						printf("new speed: %d\n", v);
						//cout << "new speed: " << v << endl;
						break;
					case 'h':
						v = max(v/32, 1);
						printf("new speed: %d\n", v);
						//cout << "new speed: " << v << endl;
						break;
					case 'u':
						v = min(2*v, SPEED_MAX);
						printf("new speed: %d\n", v);
						//cout << "new speed: " << v << endl;
						break;
					case 'e':
						v = min(32*v, SPEED_MAX);
						printf("new speed: %d\n", v);
						//cout << "new speed: " << v << endl;
						break;
					case ' ': //toggle pause
						b_play = !b_play;
						b_step = false;
						break;
					case 'p': //text display infos
						//cout << "step= " << t << endl;
						printf("step %lld (%d, %d) %d->%d x %d->%d\n", t, xi, yi, i_min_aux, i_max_aux, j_min_aux, j_max_aux);
						//cout<<"step"<<t<<" x"<<xi<<" y"<<yi<<" t"<<t<<" ia"<<i_min_aux<<" ib"<<i_max_aux<<" ja"<<j_min_aux<<" jb"<<j_max_aux<<endl;
						break;
					case 'n':
						b_play = false;
						b_step = true;
					default:
						break;
				}
			}
			if(b_exit == true) { break; }
		}
		if (t>u_t) { b_play = false; }
		if(b_exit == true) { break; }
		//}}}

		//{{{SLEEP IF TOO FAST
		chrono_temps = clock() - chrono_temps;
		a = frame_temps - chrono_temps*1'000/CLOCKS_PER_SEC;
		if (a>0) {
			SDL_Delay(a);
		} else {
			SDL_Delay(-a/5);
			//cout << "delay: " << -a << "\t";
		}
		//}}}
	}

	//{{{FREE MEMORY
	TTF_CloseFont(font);
	TTF_Quit();
	if(surface != nullptr) { SDL_FreeSurface(surface); }
	if(surface_output != nullptr) { SDL_FreeSurface(surface_output); }
	if(texture != nullptr) { SDL_DestroyTexture(texture); }
	if(renderer != nullptr) { SDL_DestroyRenderer(renderer); }
	if(win != nullptr) { SDL_DestroyWindow(win); }
	SDL_Quit();
	printf("final step: %lld, frames: %d", t, n_frame);
	//cout << "final step: " << t << endl;
	return 0;
	//}}}
}
